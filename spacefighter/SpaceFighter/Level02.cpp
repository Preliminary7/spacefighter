

#include "Level02.h"
#include "EnemyCruiser.h"


void Level02::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\EnemyCruiser.png");

	const int COUNT = 100;

	double xPositions[COUNT] =
	{
		0.55, 0.9, 0.22,
		0.25, 0.66, 0.30,
		0.60, 0.60, 0.99, 0.66, 0.92,
		0.64, 0.55, 0.55, 0.66, 0.44,
		0.55, 0.44, 0.88, 0.56, 0.11,
		0.11, 0.11, 0.22, 0.55, 0.44,
		0.11, 0.55, 0.11, 0.11, 0.22,
		0.11, 0.44, 0.55, 0.33, 0.55, 
		0.44, 0.55, 0.33, 0.55, 0.22,
		0.44, 0.33, 0.44, 0.22, 0.33,
		0.55, 0.44, 0.22, 0.33,
		0.55, 0.9, 0.22,
		0.25, 0.66, 0.30,
		0.60, 0.60, 0.99, 0.66, 0.92,
		0.64, 0.55, 0.55, 0.66, 0.44,
		0.55, 0.44, 0.88, 0.56, 0.11,
		0.11, 0.11, 0.22, 0.55, 0.44,
		0.11, 0.55, 0.11, 0.11, 0.22,
		0.11, 0.44, 0.55, 0.33, 0.55,
		0.44, 0.55, 0.33, 0.55, 0.22,
		0.44, 0.33, 0.44, 0.22, 0.33,
		0.55, 0.44, 0.22, 0.33

	};

	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		0.25,  0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25,
		0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		0.25,  0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		EnemyCruiser *pEnemy = new EnemyCruiser();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}
